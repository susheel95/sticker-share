import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
	providers: [NgbModalConfig, NgbModal],
})
export class AppComponent implements OnInit {
	title = 'sticker-share';
	selectedId: string = "";
	public timeLeft: number = 60;
	public interval: any = '60';
	public upcomingShareTime: any = '';
	public nextTime: number = 300;//86400;
	gifts: any = [1, 2, 3, 4];
	stickers: any = [];
	stickerUrl: string = '';
	public stickersUrl = [
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/Cake-min.gif",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/hohoho-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/Christmas-f-2-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/sticker_preview_087eb5b0-b3b8-4b65-b3bb-f79488a6e8f1-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/Christmas-tree_01-min.gif",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/sticker_preview_1ab629d9-00e7-4622-aa9f-32eadecedd71-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/Christmas-tree_02-min.gif",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/sticker_preview_2619ba19-9d1b-4c60-acbd-af5ca5685935-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/Santa-and-christmas",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/tree-min.gif",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/sticker_preview_27b11e63-35ef-405c-af4d-9c3f16cbea18-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/Santa-min.gif",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/sticker_preview_354a6d6e-faf2-4ab9-aea6-b1211dfd683c-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/Star-min.gif",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/sticker_preview_3a7459a7-eb1a-4bcc-b923-07241b7f5371-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/chirstmas-f-1-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/sticker_preview_8fb198ad-f0d7-4613-9a99-353c5d981821-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/christmas-genric-1-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/sticker_preview_950fe05f-5ef9-41d2-b112-5302c3241b91-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/christmas-genric-2-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/sticker_preview_ab3ad19f-561b-4820-b4ef-55fc233ffcff-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/christmas-m-2-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/sticker_preview_c09aef4f-62f1-4193-a174-6ec5d24ec7b2-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/christmas-male-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/will-you-be-my-santa-min.png",
		"https://assets.bobblekeyboard.net/christmas-gift-campaign-website/stickers/christmas-min.png",
	];

	constructor(config: NgbModalConfig, private modalService: NgbModal) {
		// customize default values of modals used by this component tree
		config.backdrop = 'static';
		config.keyboard = false;
	}

	open(content: any, id: string) {
		this.selectedId = id;
		this.stickerUrl = this.getSticker();
		this.modalService.open(content);
	}

	openTerm(content: any) {
		this.modalService.open(content);
	}

	startTimer() {
		setInterval(() => {
			this.updateCount();
		}, 1000)
	}

	pauseTimer() {
		clearInterval(this.interval);
	}

	ngOnInit() {
		this.startTimer();
	}

	updateCount() {
		if (this.timeLeft > 0) {
			this.timeLeft--;
			this.interval = this.timeLeft < 10 ? `0${this.timeLeft}` : this.timeLeft;
		} else {
			this.timeLeft = 60;
			this.interval = this.timeLeft;
		}
	}

	secondsDiff(previousDate: any, currentDate: any) {
		var t1 = new Date(previousDate);
		var t2 = new Date(currentDate);
		var dif = t1.getTime() - t2.getTime();
		var Seconds_from_T1_to_T2 = dif / 1000;
		return Math.abs(Seconds_from_T1_to_T2);
	}

	getUpcomingShareTime(date: any) {
		var t1 = new Date(date);
		t1.setSeconds(t1.getSeconds() + this.nextTime);
		var hours = t1.getHours() > 9 ? t1.getHours() : `0${t1.getHours()}`;
		var minutes = t1.getMinutes() > 9 ? t1.getMinutes() : `0${t1.getMinutes()}`;
		var seconds = t1.getSeconds() > 9 ? t1.getSeconds() : `0${t1.getSeconds()}`;
		return `${hours}:${minutes}:${seconds}`
	}

	share(url: string, content2: any) {
		var localStorageData = localStorage.getItem('s-g');
		if (localStorageData) {
			var jsonData = JSON.parse(localStorageData);
			var date = new Date().getTime();
			var secondDif = this.secondsDiff(date, jsonData.lastSharedTime);
			if (jsonData.sharedGiftIds.length == 4) {
				if (secondDif < this.nextTime) {
					this.upcomingShareTime = this.getUpcomingShareTime(jsonData.lastSharedTime)
					this.modalService.dismissAll();
					this.modalService.open(content2);
					return;
				} else {
					var jsonData: any = {
						sharedGiftIds: [this.selectedId],
						lastSharedTime: new Date().getTime()
					}
					localStorage.setItem('s-g', JSON.stringify(jsonData))
				}
			}
			if (jsonData.sharedGiftIds.indexOf(this.selectedId) == -1) {
				jsonData.sharedGiftIds.push(this.selectedId);
			}
			jsonData.lastSharedTime = new Date().getTime();
			localStorage.setItem('s-g', JSON.stringify(jsonData))
		} else {
			var jsonData: any = {
				sharedGiftIds: [this.selectedId],
				lastSharedTime: new Date().getTime()
			}
			localStorage.setItem('s-g', JSON.stringify(jsonData))
		}
		this.modalService.dismissAll();
		window.open(url, "_blank");
	}

	getSticker() {
		const random = Math.floor(Math.random() * this.stickersUrl.length);
		return this.stickersUrl[random];
	}

}
